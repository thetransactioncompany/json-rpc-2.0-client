package com.thetransactioncompany.jsonrpc2.client;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;

import static junit.framework.TestCase.*;
import static net.jadler.Jadler.*;

public class TestWithHTTPEndpoint {


        @Before
        public void setUp() {
                initJadler();
        }


        @After
        public void tearDown() {
                closeJadler();
        }


        private void stubHTTP(final JSONRPC2Request request, final JSONRPC2Response response, final int statusCode) {

                onRequest()
                        .havingMethodEqualTo("POST")
                        .havingHeaderEqualTo("Content-Type", "application/json")
                        .havingPathEqualTo("/api")
                        .havingBodyEqualTo(request.toJSONString())
                        .respond()
                        .withStatus(statusCode)
                        .withContentType("application/json")
                        .withBody(response.toJSONString());
        }


        @Test
        public void testRequest_success()
                throws Exception {

                URL url = new URL("http://localhost:" + port() + "/api");
                JSONRPC2Session session = new JSONRPC2Session(url);

                assertEquals(url, session.getURL());

                String id = "0001";
                JSONRPC2Request request = new JSONRPC2Request("ws.getName", id);
                JSONRPC2Response response = new JSONRPC2Response("MyAPI", id);

                stubHTTP(request, response, 200);

                JSONRPC2Response receivedResponse = session.send(request);

                assertTrue(receivedResponse.indicatesSuccess());
                assertEquals("MyAPI", receivedResponse.getResult());
                assertEquals(id, receivedResponse.getID());
        }


        @Test
        public void testRequest_error_with200StatusCode()
                throws Exception {

                URL url = new URL("http://localhost:" + port() + "/api");
                JSONRPC2Session session = new JSONRPC2Session(url);

                String id = "0001";
                JSONRPC2Request request = new JSONRPC2Request("ws.getName", id);
                JSONRPC2Error error = new JSONRPC2Error(55, "Invalid authentication");
                JSONRPC2Response response = new JSONRPC2Response(error, id);

                stubHTTP(request, response, 200);

                JSONRPC2Response receivedResponse = session.send(request);

                assertFalse(receivedResponse.indicatesSuccess());
                assertEquals(error, receivedResponse.getError());
                assertEquals(id, receivedResponse.getID());
        }


        @Test
        public void testRequest_error_with4xxStatusCode()
                throws Exception {

                URL url = new URL("http://localhost:" + port() + "/api");
                JSONRPC2Session session = new JSONRPC2Session(url);

                String id = "0001";
                JSONRPC2Request request = new JSONRPC2Request("ws.getName", id);
                JSONRPC2Error error = new JSONRPC2Error(55, "Invalid authentication");
                JSONRPC2Response response = new JSONRPC2Response(error, id);

                stubHTTP(request, response, 460);

                JSONRPC2Response receivedResponse = session.send(request);

                assertFalse(receivedResponse.indicatesSuccess());
                assertEquals(error, receivedResponse.getError());
                assertEquals(id, receivedResponse.getID());
        }
}
